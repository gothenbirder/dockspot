Hamnguide & Dockspot
=====================

# Hamnguide

## General

- old technology; not HTTPS: bad impression with current browsers because of
  warnings
- search utterly confusing!
- map with search results no less confusing
- uneven visual impression, too full a page

## App

- just a web page in an app; but maybe works for now
- feels old fashioned

## Backend

- not particularly userfriendly

# Dockspot

## General

- ugly -- why pink?! ... but probably unchangeable corp. design
- sure about: web first? The mobile experience (look into user statistics: how
  many are accessing the booking via mobile device?) is that of a crammed down
  web page. Though the berth choosing is kind of sweet on mobile, it's also a
  little pathetic. Also: no zooming of the harbour map in neither the desktop
  nor the mobile web page.
- confirmation mail
 - no checking of validity of mail address / validity of sign up
  - chosen password in email

## Booking

- heart of site obviously: the booking window in each harbour's page
  - uncomfortable: choose a date, get informed of harbour's _general_
    unavailability afterwards: should be different
  - message "not available for your boat size" -- what does that mean? I
    have in my profile a sailing boat 4,30 m long, 1,82 m wide with 11 cm
    depth ...
  - decimal point not handled well: I could easily enter 4,30 m, which is
    silently interpreted as 430 m; no wonder there's no room for that yacht!
  - in 'Marina Info': what does Spots and Bookable mean? What is the
    difference? Is 'Bookable' updated somehow, or fixed? (not obvious to
    user)
  - "Tip: Finding it difficult to find a slot for several days? Book one day at
    a time to find berths close to one another for the full duration of your
    stay." No. Nooooo. This is kind of desperate a solution. Not Tec-worthy.

## Testing

- can we have some 'test account', where we can try out the current feel and
  workflow without in fact booking?
