(require '[lumo.build.api :as b])

(b/build
  (b/inputs "create-users.cljs")
  {:main 'com.technocreatives.ghg.create-users
   :target :nodejs
   :output-to "out/create-users.js"})
