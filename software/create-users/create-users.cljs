(ns com.technocreatives.ghg.create-users)

(enable-console-print!)

(def yaml (js/require "js-yaml"))
(def fs (js/require "fs"))

(def SDK (js/require "directus-sdk-javascript/remote"))
(def url "https://dev.gasthamnsguiden.se/directus/api/1.1/")
; access token for an admin
(def access-token "dpBcHUC2EuexPxmItEUbYpSMIqd2grYM")
(def marina-owner-group 5)

(def client (SDK. (clj->js {:url url :accessToken access-token})))


; extract record id from directus record
; 'record' is a js object
(defn extract-id
  [record]
    (let [{{id :id} :data} (js->clj record :keywordize-keys true)]
     id))

; sets user-id for existing marina record
(defn update-marina
  [id user-id]
  (let [data {:user user-id}]
    (.then (.updateItem client "marinas" id (clj->js data)))))

; creates marina record, and updates it immediately for correct user-id
; (after creation user-id entered into DB by directis is always that of
;  the creator)
(defn create-marina
  [name user-id]
  (let [marina {:name name
                :status "1"}]
    (.then (.createItem client "marinas" (clj->js marina))
           #(update-marina (extract-id %) user-id))))

(defn create-marinas
  [names user-id]
  (dorun (map create-marina names (repeat user-id))))

(def filename "marinas.yml")
(def file (.readFileSync fs filename))
(def doc (js->clj (.safeLoadAll yaml file) :keywordize-keys true))

;  (.then (.createUser client (clj->js (conj user {:group marina-owner-group})))
;         #(create-marina "Kungsö" (extract-id %))) 

(defn create-user
  [marina-set]
  (let [{user :user} marina-set
        {marinas :marinas} marina-set]
  (.then (.createUser client (clj->js (conj user {:group marina-owner-group})))
         #(create-marinas marinas (extract-id %)))))

(dorun (map create-user doc))
