# Open Source Headless CMS

## eZ Platform?


## Comparison

|name		| technology		| comments					|
|---------------|-----------------------|-----------------------------------------------| 
|[directus](https://getdirectus.com/)	| MySQL, PHP		| kind of mature, confusing rights management, no Swedish UI, not multilingual per se 	|
|[cockpit](https://getcockpit.com/)	| SQLite / MongoDB, PHP	| promising, rights management??		|
|[rooftop](https://www.rooftopcms.com/)	| Wordpress / S3(?!)	| alleviates Wordpress weirdness (?), seems to insist on saving assest in S3 :-O |
|[blackstar](https://blackstarcms.net/)	| nodejs / SQLite	| nice, but no user management			|
|[enduro.js](https://www.endurojs.com/)	| nodejs / express, flatfile / mongodb	| multilingual, no REST API: rather an express extension |
|[keystone.js](http://keystonejs.com/)	| express, mongodb	| widely used, no REST API: rather an express extension |
|[relax](https://github.com/relax/relax)	| MongoDB, React, node	| pre-beta, sadly; exposes GraphQL API :-)	|
|[grav](https://getgrav.org/)		| PHP, flatfiles	| plugin for REST API available, but dev ceased	|
|[respond 6](https://respondcms.com/)	| PHP, flatfiles	| said to expose REST API; couldn't find it though |
|[eZ Platform](https://ezplatform.com/)	| PHP, MySQL/Maria	| beast of a CMS, complete, including JS client; GraphQL plugin available; probably non-trivial to setup / use |
| [expressa](https://github.com/thomas4019/expressa)	| Express, MongoDB / PostgresQL / flatfile | API middleware for Express -- worth a deepter look? |
| [drupal](https://www.drupal.org/)	| PHP, MqSQL / Maria / PostgreSQL / SQLite ... | huge, established, includes REST API and GraphQL API out of the box |


## Interesting, but ...

[Django-like for Clojure, with GraphQL](https://cheesecakelabs.com/blog/case-clojure-graphql-replacing-django/)
